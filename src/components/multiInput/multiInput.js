import { Button} from 'react-bootstrap';
import { WithContext as ReactTags } from 'react-tag-input';
import { useMultiInput } from './hooks';
import 'bootstrap/dist/css/bootstrap.css';

import './style.css';

export const MultiInput = ({label, name, onChange}) => {

    const KeyCodes = {
        comma: 188,
        enter: 13
      };
      
    const delimiters = [KeyCodes.comma, KeyCodes.enter];

    const [tags,
        onItemUpdate,
        handleDelete,
        handleAddition
    ] = useMultiInput(onChange);

    return (
        <div className="tag-app">
        <ReactTags
            tags={tags}
            delimiters={delimiters}
            handleDelete={handleDelete}
            handleAddition={handleAddition}
            inputFieldPosition="bottom"
      />
       <Button variant="primary" onClick={onItemUpdate} >Submit</Button>
      </div>
    )
}