import {useState, useEffect} from 'react';

export const useMultiInput = (onChange) => {
    
    const [tags, setTags] = useState([]);

    const onItemUpdate = () => {
        const ids = tags.map(item => item.id)
        onChange(ids);
    }

    const handleDelete = i => {
        setTags(tags.filter((tag, index) => index !== i));
      };
    
      const handleAddition = tag => {
        setTags([...tags, tag]);
      };
    

    return [
        tags,
        onItemUpdate,
        handleDelete,
        handleAddition
    ]
}